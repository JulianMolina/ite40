AWS
- Amazon Web Services INC. Provides a computing platforms and API's to Individuals and Businesses. The major upside to this service is the pay as you go basis. AWS was launched on March 3, 2006 as a subsidiary of Amazon. AWS is used mostly by third party for cost-effective cloud computing solutions.
---
DevOPS
- DevOPS is a model for delivering an aplication Efficiently and Effectively. The process allows programmers to Collaborate and work together better. There is many different alleged stories of the operation model. "But the most common one is when a frustrated consultant, project manager, and agile practitioner, Patrick Debois, got fed up with the separation between software development and IT operations and the inefficiencies it raised." ![Source](https://www.bunnyshell.com/blog/history-of-devops)
---
![Model](https://d1.awsstatic.com/product-marketing/DevOps/DevOps_feedback-diagram.ff668bfc299abada00b2dcbdc9ce2389bd3dce3f.png)
---
<br>
Fallon
<br>

ORM
# ORM stands for Object-relational Mapper Software

# In 1995, the first successful ORM framework that was commercially available was published. This framework was called TOPLink, a product by Smalltalk

# In 1996 a Java version of TopLink was added and was called TopLink for Java

# In 1997 the first company to buy TopLink for Java was 3M

# ORM can be seen as a layer that connects object-oriented programs (OOP) to relational databases

# An ORM tool is software that is meant to help OOP developers interact with relational databases


SQLAlchemy
# SQLAlchemy is a ORM that uses Python and gives you tools for working with and accessing data in a database

# The Original Author of SQLAlchemy is Michael Bayer

# SQLAlchemy was released February 14, 2006.