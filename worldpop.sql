DROP TABLE IF EXISTS worldpop;
CREATE TABLE WorldPop
 ( id SERIAL, Country TEXT, Capital TEXT, Continent TEXT, Population BIGINT, PRIMARY KEY (id) );

SELECT * FROM Worldpop;

SELECT * FROM Worldpop
WHERE Continent = 'Asia' and Population > 1000000000
ORDER BY Population;
