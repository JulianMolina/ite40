# Summary
- Dee begins discovering the wonders of postgres. After asking their boss for help getting started and getting a passive aggressive response, Dee decides to take matters into their own hands. Dee is inexperienced with non-gui psql so he begins to learn running postgres from the CLI. Afterward he decides to begin learning how to load a CSV into postgres. 

# Stuff I don't know
- Navicat
- SQLPro
- Postico
- PG Admin IV
- idempotent
- Schema
## Commands I don't know
- \h = help
### Table Creation
'drop table if exists master_plan;
create table master_plan( start_time_utc text, duration text,
date text,
team text,
spass_type text,
target text, request_name text, library_definition text, title text,
description text
);'

### Schema Creation
'create schema if not exists import;
drop table if exists import.master_plan; create table import.master_plan(
start_time_utc text, duration text,
date text,
team text, spass_type text, target text, request_name text,
);
COPY import.master_plan
FROM '[PATH TO]/master_plan.csv' WITH DELIMITER ',' HEADER CSV;'


  

