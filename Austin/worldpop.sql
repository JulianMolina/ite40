DROP TABLE IF EXISTS worldpop;
CREATE TABLE WorldPop
 ( id SERIAL, Country TEXT, Capital TEXT, Continent TEXT, Population TEXT, PRIMARY KEY (id) );

\COPY WorldPop (Country, Capital, Continent, Population)FROM 'WorldPop.csv' WITH DELIMITER ',' CSV HEADER;

SELECT * FROM Worldpop;

SELECT * FROM Worldpop
WHERE Continent = 'Asia'
ORDER BY Country;
