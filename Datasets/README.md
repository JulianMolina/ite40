# Source For Database

---
[World Population](https://www.kaggle.com/datasets/iamsouravbanerjee/world-population-dataset?resource=download)
---

## This data set is full of world population numbers spanning from 1970.
 This dataset includes Area Size of the Country/Territory, Name of the Continent,
 Name of the Capital, Density, Population Growth Rate, Ranking based on<br>
 Population, World Population Percentage, etc.

