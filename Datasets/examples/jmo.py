#3+4
a = 3
b = 4
print(a + b)

#print 5
c = 5
print(c)
c = "Five"
print(c)

#float vs int
print(4)
print(4.4)

#boolean
4 == 5
print(bool(0))
3 != 4
3 < 4
3 <= 4

#strings
print("Hello \"Jeff\"."+" How are you doing?")


