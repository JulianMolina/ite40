# Pandas Definitions

1. Jupyter Notebooks
- Formerly Ipython Notebooks, Juypyter notebooks is an open sourced,  web based programming environment used to make notebook documents.<br>
---
2. Pandas
- Pandas is an free software created for pythonby Wes Mckinny to manipulate data and analyze it. It's biggest use is for messing with tables and time series'.<br>
---
3. Data Sets
- Collections of data that are used to set lists values to variables. For tabular data variable values are displayed in rows and columns.<br>
---
4. Statistics
- Statistics is the study of collection, organization, analysis, interpration, and presentation of data. Statistics are mostly used for industrial, scientific, or social problems.<br>
---
5. Time Series
- In mathmatics a time series is data points indexed in time order. The time series database is a software used for storing and serving time series through matching pairs of variables and values.<br>
---
6. Vectorization
- In mathmatics vectorization is the linear transformation that converts a matrix into a column vector.<br>
---
7. Data Alignment
- Data Alignment is the order in which computers read and compute data. Data that lines up with the bytes a CPU can read are processed more efficently.<br>
---
8. Cleaning Data
- Data cleansing is the process of removing corrupt or wrong data from a Data set. Also, data cleansinf includes taking irrelevant data and replacing it with more accuracte data.<br>
---
9. Aggregation
- Aggregation is the process of grouping together multiple rows of values to form one singular summary value.<br>
---
10. Descriptive Statistics
Discriptive Statics are statistics that are grouped together to convey a summary of the statistics instead of learning about the sample of data.<br>
---
11. Visualization
- Creating an image, diagram, or animation in order to convey a message or show how something would look.<br>
---
<br>

# Interface
- The definition for the word interface is something that interacts with something else. In computing, an interface is a boundary in which two computer components communicate and share data.<br>
 Some interfaces send and recieve data like touch screens. Other interfaces only send data like mouses and keyboards. Hardware interfaces are described by the mechanical, electrical,<br>
 and logical signals and how they are sequenced. Software interfaces allow you to acsess other computer resources like memory and cpu. User interfaces allow communication between a computer and<br>
 humans. One popular type of user interface is the dreaded Graphic user interface. Another is the Command Line Interface that can be more efficent with the proper know how.
