# Notes

### Numpy
1. Numpy is the core package for scientific computing in Python.
2. Numpy is core to learning Pandas later on.
----------------------------------------------------------------------------------------------------------------
### Numpy Arrays
3. To add a number to every element in a nested list use the Matrix list
 comprehension.(Although it is not very readable.)
4. A Numpy array is an N-dimensional array for data that is the same.
5. 1 dimensional array = one axis.
---------------------------------------------------------------------------------------------------------------
![oneDimension](https://learning.oreilly.com/api/v2/epubs/urn:orm:book:9781492080992/files/assets/pyfe_0401.png)
---------------------------------------------------------------------------------------------------------------
6. 2 dimensional array's have explicit column and row orientation.
---------------------------------------------------------------------------------------------------------------
![2Dimensions](https://learning.oreilly.com/api/v2/epubs/urn:orm:book:9781492080992/files/assets/pyfe_0401.png)
---------------------------------------------------------------------------------------------------------------
7. Becuase the datatypes are the same Numpy forces the the data type to be a float64
----------------------------------------------------------------------------------------------------------------
### Vectorization and Broadcasting
8. If you build the sum of a scalar on a Numpy array, Numpy will perform a element wise operation known as
vectorization.
9. Scalar is a basic data type like a float or string.
----------------------------------------------------------------------------------------------------------------
### Universal Functions
10. Universal Functions work on every element in an array.
11. If you use standard python sqaure root function you will get an error but if you nest it in a numpy loop to
 get every element it will work.
----------------------------------------------------------------------------------------------------------------
### Creating and Manipulating Arrays
12. With nested lists you can use chained indexing but in Numpy arrays you can provide the index and slice
 arguements for both dimensions.
13. One way to easily organize contructors is the arange function.(This returns a numpy array)
---------------------------------------------------------------------------------------------------------------

