import pandas as pd

from pathlib import Path

df = pd.read_excel("/Users/julianmolina/ite40/Datasets/Tables/PivotPop.xlsx")

print=df.info()

# Directory of this file
this_dir = Path(__file__).resolve().parent

# Read in all Excel files from all subfolders of sales_data
parts = []
for path in (this_dir / "sales_data").rglob("*.xls*"): 2
print(f'Reading {path.name}')
part = pd.read_excel(path, index_col="transaction_id")
parts.append(part)

# Combine the DataFrames from each file into a single DataFrame
# pandas takes care of properly aligning the columns
df = pd.concat(parts)

# Pivot each store into a column and sum up all transactions per date
pivot = pd.pivot_table(df,index="transaction_date", columns="store",values="amount", aggfunc="sum")

# Resample to end of month and assign an index name
summary = pivot.resample("M").sum()
summary.index.name = "Month"

# Write summary report to Excel file
summary.to_excel(this_dir / "sales_report_pandas.xlsx")

#cell range
df = pd.read_excel("xl/stores.xlsx",sheet_name="2019", skiprows=1, usecols="B:F")

#Converter Function
def fix_missing(x):return False if x in ["", "MISSING"] else x
df = pd.read_excel("xl/stores.xlsx",sheet_name="2019", skiprows=1, usecols="B:F",converters={"Flagship": fix_missing})

#value and name
heets = pd.read_excel("xl/stores.xlsx", sheet_name=["2019", "2020"],skiprows=1, usecols=["Store", "Employees"])sheets["2019"].head(2)

#header = none
df = pd.read_excel("xl/stores.xlsx", sheet_name=0,skiprows=2, skipfooter=3,usecols="B:C,F", header=None,names=["Branch", "Employee_Count", "Is_Flagship"])

#Handeling of NAN values
df = pd.read_excel("xl/stores.xlsx", sheet_name="2019",skiprows=1, usecols="B,C,F", skipfooter=2,na_values="MISSING", keep_default_na=False)

#Openeing and Closing Text Files
f = open("output.txt", "w").write("Some text").close()

#appending
with open("output.txt", "w") as f:f.write("Some text")

#Excel File class
with pd.ExcelFile("xl/stores.xls") as f:
             df1 = pd.read_excel(f, "2019", skiprows=1, usecols="B:F", nrows=2)
             df2 = pd.read_excel(f, "2020", skiprows=1, usecols="B:F", nrows=2)

stores = pd.ExcelFile("xl/stores.xlsx").stores.sheet_names

#Read from url
url = ("https://raw.githubusercontent.com/fzumstein/""python-for-excel/1st-edition/xl/stores.xlsx").pd.read_excel(url, skiprows=1, usecols="B:E", nrows=2)

#excel writer
with pd.ExcelWriter("written_with_pandas2.xlsx") as writer:
             df.to_excel(writer, sheet_name="Sheet1", startrow=1, startcol=1)
             df.to_excel(writer, sheet_name="Sheet1", startrow=10, startcol=1)
             df.to_excel(writer, sheet_name="Sheet2")



                