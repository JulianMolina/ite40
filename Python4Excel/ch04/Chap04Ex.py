#Examples From Chapter Four

#matrix = [[1, 2, 3],
          [4, 5, 6],
          [7, 8, 9]]
print([[i + 1 for i in row] for row in matrix])

import numpy as np
array1 = np.array([10, 100, 1000.])
array2 = np.array([[1., 2., 3.],
                   [4., 5., 6.]])

print(array1.dtype)
print(float(array1[0]))

print(array2 +1)

print(array2 * array2)
print(array2 * array1)

print(array2 @ array2.T)

print(np.array([[math.sqrt(i) for i in row] for row in array2]))

print(np.sqrt(array2))

print(array2.sum(axis=0))

print(array2.sum())

print(array1[2])

print(array2[0, 0])

print(array2[:, 1:])

print(array2[:, 1])

print(array2[1, :2])

print(np.arange(2 * 5).reshape(2, 5))

print(np.random.randn(2, 3))

print(subset = array2[:, :2]
         subset))#





